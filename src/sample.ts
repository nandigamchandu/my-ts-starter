// All
export const all: <T>(arr: T[], fn?: (value: T) => boolean) => boolean = (
  arr,
  fn = Boolean
) => arr.every(fn);
